import { Scene, Camera, Nullable, Observer, Engine } from '@babylonjs/core';

export enum ADJUSTMENT {
    HORIZONTAL,
    VERTICAL,
    BOTH
};

let observer: Nullable<Observer<Engine>>;

/**
 * Adjusts camera field of view to ensure that object size remains relatively consistent to window size
 * @param scene Current Babylon scene
 * @param camera Current Babylon camera
 */
function adjustCamera(
    scene: Scene, 
    camera: Camera,
    adjustmentType: ADJUSTMENT
    ) {
    switch (adjustmentType) {
        case ADJUSTMENT.HORIZONTAL: {
            camera.fovMode = Camera.FOVMODE_HORIZONTAL_FIXED;
            break;
        }
            
        case ADJUSTMENT.VERTICAL: {
            camera.fovMode = Camera.FOVMODE_VERTICAL_FIXED;
            break;
        }
            
        case ADJUSTMENT.BOTH: {
            if (scene.getEngine().getRenderHeight() > scene.getEngine().getRenderWidth()) {
                camera.fovMode = Camera.FOVMODE_HORIZONTAL_FIXED;
            }
            else {
                camera.fovMode = Camera.FOVMODE_VERTICAL_FIXED;
            }
            break;
        }
    }
}

/**
 * Registers an observer that automatically adjusts camera field of view on window resize
 * @param scene Current Babylon scene
 * @param camera Current Babylon camera
 */
export function setupCameraAutoAdjust(
    scene: Scene, 
    camera: Camera, 
    adjustmentType = ADJUSTMENT.BOTH
    ) {
    if (observer) scene.getEngine().onResizeObservable.remove(observer);

    observer = scene.getEngine().onResizeObservable.add(() => {
        adjustCamera(scene, camera, adjustmentType);
    });
    adjustCamera(scene, camera, adjustmentType);
    return observer;
}

